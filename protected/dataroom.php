<?php
error_reporting(0);
ini_set('display_errors', 0);

 require_once('../config.php');
require_once(BASE_PATH.'/manage-site/manage-site-common.php');
    //ini_set('include_path',BASE_PATH.'/includes/');
    if(($loginUrl = checkLogin()) !== true) { header('Location: '.$loginUrl); exit; }

// from http://uk.php.net/filesize
function format_bytes($a_bytes)
{
    if ($a_bytes < 1024) {
        return $a_bytes .' B';
    } elseif ($a_bytes < 1048576) {
        return round($a_bytes / 1024, 2) .' KB';
    } elseif ($a_bytes < 1073741824) {
        return round($a_bytes / 1048576, 2) . ' MB';
    } elseif ($a_bytes < 1099511627776) {
        return round($a_bytes / 1073741824, 2) . ' GB';
    } elseif ($a_bytes < 1125899906842624) {
        return round($a_bytes / 1099511627776, 2) .' TB';
    } elseif ($a_bytes < 1152921504606846976) {
        return round($a_bytes / 1125899906842624, 2) .' PB';
    } elseif ($a_bytes < 1180591620717411303424) {
        return round($a_bytes / 1152921504606846976, 2) .' EB';
    } elseif ($a_bytes < 1208925819614629174706176) {
        return round($a_bytes / 1180591620717411303424, 2) .' ZB';
    } else {
        return round($a_bytes / 1208925819614629174706176, 2) .' YB';
    }
}

function outputFiles($path)
{
    $output = '';
    //$handle = @opendir($path);
    //while (false !== ($entry = @readdir($handle))) {
    //    if ($entry != '.' && $entry != '..') {
    //        $output .= '<li><a href="'.SITE_URL.'/protected/download.php?file=protected/'.urlencode($path).'/'.urlencode($entry).'" title="'.htmlspecialchars($entry).'" target="_blank">'.htmlspecialchars($entry.' - '.format_bytes(filesize($path.'/'.$entry))).'</a></li>';
    //    }
    //}
    foreach (glob($path.'/*.*') as $filename) {    	
        $outputFilename = str_replace($path.'/', '', $filename);
        $output .= '<li><a href="'.SITE_URL.'/protected/download.php?file=protected/'.urlencode($path).'/'.urlencode($outputFilename).'" title="'.htmlspecialchars($outputFilename).'" target="_blank">'.htmlspecialchars($outputFilename.' - '.format_bytes(filesize($filename))).'</a></li>';
    }
    if ($output == '') {
        $output .= '<li>Sorry, no files currently; please check back soon</li>';
    }
    echo $output;
}

function outputImages($path)
{
    $output = '';
    //$handle = @opendir($path);
    //while (false !== ($entry = @readdir($handle))) {
    //    if ($entry != '.' && $entry != '..') {
    //        $output .= '<li><a href="'.SITE_URL.'/protected/download.php?file=protected/'.urlencode($path).'/'.urlencode($entry).'" title="'.htmlspecialchars($entry).'" target="_blank">'.htmlspecialchars($entry.' - '.format_bytes(filesize($path.'/'.$entry))).'</a></li>';
    //    }
    //}
    foreach (glob($path.'/*.*') as $filename) {  
        $outputFilename = str_replace($path.'/', '', $filename);
        $output .= '<li><a href="'.SITE_URL.'/protected/download.php?file=protected/'.urlencode($path).'/large/'.urlencode($outputFilename).'" title="'.htmlspecialchars($outputFilename).'" target="_blank"><img src="'.SITE_URL.'/protected/download.php?file=protected/'.urlencode($path).'/'.urlencode($outputFilename).'" alt="'.htmlspecialchars($outputFilename).'"></a></li>';
    }
    if ($output == '') {
        $output .= '<li>Sorry, no files currently; please check back soon</li>';
    }
    echo $output;
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>
<?=$title;?>
- Data room - Commercial Property Agency &amp; Consultancy <?= $client ?></title>
<meta name="keywords" content="<?= $client ?>">
<meta name="description" content="Data rooms for Commercial Property Agency &amp; Consultancy <?= $client ?>">
<link href="assets/bootstrap/css/bootstrap.css" rel="stylesheet">
<link href="css/custom.css" type="text/css" rel="stylesheet" />

<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
	  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

<script type="text/javascript" src="//use.typekit.net/rww1sfn.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
</head>

<body class="nobg">
<nav>
<div class="container">
<div class="row">
<div class="span6"> <img src="images/<?= $logoSmall ?>" alt="<?= $client ?> - Commercial Property Agency &amp; Consultancy"/>
    <h1><span>Dataroom:</span>
        <?=$shortTitle;?>
    </h1>
</div>
<div class="span6">
<ul>
<li><a href="http://www.hurstwarne.co.uk" target="_blank"><?= $client ?> Home</a></li>
<li><a href="mailto:<?= $mailto ?>?subject=<?= $client ?> Chartered Surveyors  - <?= $shortTitle ?> - Enquiry" class="contact highlight" title="<?= $client ?> Chartered Surveyors - Contact us">Contact Us</a></li>
<li><a href="/index.php?logout=1" class="contact" title="<?= $client ?> Chartered Surveyors - Log Out" >Log Out</a></li>
</div>
</div>
</div>
</nav>
<div class="container">
    <div class="row">
        <div class="span3">
            <?php //$sections = array('architect_plans' => 'Architect Plans'); ?>
            <ul class="nav nav-tabs nav-stacked" id="myTab">
                <li>
                    <h2>Dataroom</h2>
                </li>
                <li class="active top top-level"><a href="#tab1">1. Cross Sections</a></li>
                <li class="top-level"><a href="#tab2">2. Elevations</a></li>
                <li class="top-level"><a href="#tab3">3. Floor Plans</a></li>
                <li class="top-level"><a href="#tab4">4. Planning (Permitted Development)</a></li>
                <li class="top-level"><a href="#tab5">5. Planning (Rear Site)</a></li>
                <li class="top-level"><a href="#tab6">6. Property Details</a></li>
                <li class="top-level"><a href="#tab7">7. Reversionary Lease</a></li>
                <li class="top-level"><a href="#tab8">8. Site Plan</a></li>
                <li class="top-level"><a href="#tab9">9. Title Infomation</a></li>
                <li class="top-level"><a href="#tab10">10. Topo Survey</a></li>
                <li class="top-level"><a href="#tab11">11. EPCs</a></li>
                <li class="top-level"><a href="#tab12">12. Asbestos Survey</a></li>
            </ul>
            <p class="added">You will need to have adobe acrobat installed to view some of these documents, <a href="http://www.adobe.com/products/acrobat.html" target="_blank">click here</a> if you do not have it installed</p>
            <!--<a class="btnD" href="downloads/Keybridge-Brochure.pdf">Download Brochure</a>--> 
        </div>
        <div class="span9">
            <?php //foreach($sections as $ref => $title) echo '<a href="#'.urlencode($ref).'">'.htmlspecialchars($title).'</a>'; ?>
            <div class="tab-content">

                <div class="tab-pane active" id="tab1">
                    <h2>1. Cross Sections</h2>
                        <ul>
                            <?php outputFiles('download/1. Cross Sections'); ?>
                        </ul>
                </div>

                <div class="tab-pane" id="tab2">
                    <h2>2. Elevations</h2>
                    <ul>
                        <?php outputFiles('download/2. Elevations'); ?>
                    </ul>
                </div>

                <div class="tab-pane" id="tab3">
                    <h2>3. Floor Plans</h2>
                    <ul>
                        <?php outputFiles('download/3. Floor Plans'); ?>
                    </ul>
                </div>

                <div class="tab-pane" id="tab4">
                    <h2>4. Planning (Permitted Development)</h2>
                    <ul>
                        <?php outputFiles('download/4. Planning (Permitted Development)'); ?>
                    </ul>
                </div>

                <div class="tab-pane" id="tab5">
                    <h2>5. Planning (Rear Site)</h2>
                    <ul>
                        <?php outputFiles('download/5. Planning (Rear Site)'); ?>
                    </ul>
                </div>

                <div class="tab-pane" id="tab6">
                    <h2>6. Property Details</h2>
                    <ul>
                        <?php outputFiles('download/6. Property Details'); ?>
                    </ul>
                </div>

                <div class="tab-pane" id="tab7">
                    <h2>7. Reversionary Lease</h2>
                    <ul>
                        <?php outputFiles('download/7. Reversionary Lease'); ?>
                    </ul>
                </div>

                <div class="tab-pane" id="tab8">
                    <h2>8. Site Plan</h2>
                    <ul>
                        <?php outputFiles('download/8. Site Plan'); ?>
                    </ul>
                </div>

                <div class="tab-pane" id="tab9">
                    <h2>9. Title Infomation</h2>
                    <ul>
                        <?php outputFiles('download/9. Title Infomation'); ?>
                    </ul>
                </div>

                <div class="tab-pane" id="tab10">
                    <h2>10. Topo Survey</h2>
                    <ul>
                        <?php outputFiles('download/10. Topo Survey'); ?>
                    </ul>
                </div>

                <div class="tab-pane" id="tab11">
                    <h2>11. EPCs</h2>
                    <ul>
                        <?php outputFiles('download/11. EPCs'); ?>
                    </ul>
                </div>

                 <div class="tab-pane" id="tab12">
                    <h2>12. Asbestos Survey</h2>
                    <ul>
                        <?php outputFiles('download/12. Asbestos Survey'); ?>
                    </ul>
                </div>

            </div>
        </div>
    </div>
</div>
<!--JQUERY--> 
<script type="text/javascript" src="js/jquery.js"></script> 
<!--BOOTSTRAP--> 
<script type="text/javascript" src="assets/bootstrap/js/bootstrap.js"></script> 
<script>
		$(function () {
			$('#myTab a').click(function (e) {
			e.preventDefault();
			$(this).tab('show');
			});	
		});
	</script> 
<script>
		// Javascript to enable link to tab
		var url = document.location.toString();
		if (url.match('#')) {
			$('.nav-tabs a[href=#'+url.split('#')[1]+']').tab('show') ;
		} 
		
		// Change hash for page-reload
		$('.nav-tabs a').on('shown', function (e) {
			window.location.hash = e.target.hash;
		});
	</script>
</body>
</html>
